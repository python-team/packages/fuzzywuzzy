fuzzywuzzy (0.18.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-levenshtein.
    + python3-fuzzywuzzy: Drop versioned constraint on python3-levenshtein in
      Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 03:08:32 +0100

fuzzywuzzy (0.18.0-3) unstable; urgency=medium

  * Switch from nose to unittest for testing (Closes: #1018362).
  * Update Standards-Version.
  * Update copyright year.
  * Build-Depends: Add <!nocheck> to python3-pycodestyle.
  * Drop "This package contains fuzzywuzzy for Python 3." from description.

 -- Edward Betts <edward@4angle.com>  Sun, 28 Aug 2022 20:30:10 +0100

fuzzywuzzy (0.18.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Edward Betts ]
  * Use dh-sequence-python3.
  * Update Standards-Version.
  * Update copyright year.
  * Update debhelper-compat to 13.
  * Update debian/watch file format version.
  * Remove fuzzywuzzy.egg-info during clean target.

 -- Edward Betts <edward@4angle.com>  Fri, 12 Mar 2021 07:53:44 +0000

fuzzywuzzy (0.18.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.

  [ Edward Betts ]
  * New upstream release.
  * Update Standards-Version.
  * Set Rules-Requires-Root to no.
  * Update copyright year.

 -- Edward Betts <edward@4angle.com>  Fri, 13 Mar 2020 08:04:07 +0000

fuzzywuzzy (0.17.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Edward Betts ]
  * Drop python2 support. (Closes: #936574)
  * Update Standards-Version (no changes).
  * Update debhelper-compat to 12.

 -- Edward Betts <edward@4angle.com>  Fri, 30 Aug 2019 12:15:24 +0100

fuzzywuzzy (0.17.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).

 -- Edward Betts <edward@4angle.com>  Thu, 08 Nov 2018 16:55:25 +0000

fuzzywuzzy (0.16.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Edward Betts ]
  * d/tests/control: switch to using Test-Command
  * d/control: update Standards-Version (no changes)

 -- Edward Betts <edward@4angle.com>  Sat, 19 May 2018 09:15:01 +0200

fuzzywuzzy (0.16.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).
  * Update debian/compat to 11.
  * Use https in debian/watch.
  * Update years in debian/copyright.
  * Remove trailing space from debian/changelog entries.

 -- Edward Betts <edward@4angle.com>  Tue, 16 Jan 2018 07:28:15 +0000

fuzzywuzzy (0.15.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).

 -- Edward Betts <edward@4angle.com>  Thu, 05 Oct 2017 09:04:41 +0100

fuzzywuzzy (0.15.0-2) unstable; urgency=medium

  * Correct package description (Closes: #858487)

 -- Edward Betts <edward@4angle.com>  Mon, 17 Apr 2017 18:06:00 +0100

fuzzywuzzy (0.15.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Mon, 17 Apr 2017 17:20:42 +0100

fuzzywuzzy (0.14.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Fri, 11 Nov 2016 18:46:34 +0000

fuzzywuzzy (0.11.1-2) unstable; urgency=medium

  * fix DEP-8 tests to use pycodestyle instead of pep8

 -- Edward Betts <edward@4angle.com>  Wed, 31 Aug 2016 21:25:33 +0100

fuzzywuzzy (0.11.1-1) unstable; urgency=medium

  * New upstream release.
  * Switch from pep8 to pycodestyle.

 -- Edward Betts <edward@4angle.com>  Wed, 31 Aug 2016 11:43:28 +0100

fuzzywuzzy (0.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).
  * Minor change to package description.
  * Add details of StringMatcher.py license.

 -- Edward Betts <edward@4angle.com>  Mon, 04 Jul 2016 08:03:56 +0100

fuzzywuzzy (0.10.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Mon, 21 Mar 2016 17:14:04 +0000

fuzzywuzzy (0.8.1-4) unstable; urgency=medium

  * debian/tests/control: fix Depends, pep8 module is now in python-pep8

 -- Edward Betts <edward@4angle.com>  Fri, 11 Mar 2016 07:10:55 +0000

fuzzywuzzy (0.8.1-3) unstable; urgency=low

  * debian/control: update Build-Depends, pep8 module is now in python-pep8

 -- Edward Betts <edward@4angle.com>  Thu, 10 Mar 2016 09:24:33 +0000

fuzzywuzzy (0.8.1-2) unstable; urgency=low

  * debian/rules: fix invalid Vcs-Git URL

 -- Edward Betts <edward@4angle.com>  Tue, 23 Feb 2016 07:15:32 +0000

fuzzywuzzy (0.8.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: remove unneeded versions on Build-Depends
  * debian/control: update Standards-Version (no changes)
  * debian/control: update Vcs-Git field to use https

 -- Edward Betts <edward@4angle.com>  Sun, 21 Feb 2016 07:15:00 +0000

fuzzywuzzy (0.8.0-2) unstable; urgency=medium

  * debian/tests/control: pep8 packages are required for DEP 8 tests

 -- Edward Betts <edward@4angle.com>  Tue, 22 Dec 2015 13:47:32 +0000

fuzzywuzzy (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Add autopkgtest testsuite.

 -- Edward Betts <edward@4angle.com>  Wed, 02 Dec 2015 12:30:33 +0000

fuzzywuzzy (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-* control headers.

 -- Edward Betts <edward@4angle.com>  Mon, 19 Oct 2015 14:00:54 +0100

fuzzywuzzy (0.6.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control: add a longer extended description

 -- Edward Betts <edward@4angle.com>  Tue, 08 Sep 2015 10:34:12 +0100

fuzzywuzzy (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Move to unstable. This package depends on python-levenshtein >= 0.12.0,
    which recently moved from experimental to unstable.

 -- Edward Betts <edward@4angle.com>  Mon, 17 Aug 2015 15:34:39 +0200

fuzzywuzzy (0.5.0-1) experimental; urgency=low

  * Initial release. (Closes: #781282)

 -- Edward Betts <edward@4angle.com>  Thu, 26 Mar 2015 21:20:50 +0000
